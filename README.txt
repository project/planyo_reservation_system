PLANYO.COM ONLINE RESERVATION SYSTEM - Drupal 8 module

This module embeds the Planyo.com online reservation system. Before using it, you'll need to create an
 account at planyo.com. Please see http://www.planyo.com/drupal-reservation-system for more info.

Copy all files to a 'planyo' directory inside 'modules', then enable the module on the Extend page, and configure it. The booking process will be created with the path /planyo but you can also add any number of blocks to your web site.